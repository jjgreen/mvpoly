function test_mvpoly_cube_multiply()
% test suite for @MVPolyCube/mtimes

    test_example_A;
    test_distribute_over_eval;

end

function test_example_A()
    [x, y] = MVPolyCube.variables;
    r = (1 + x) * (1 + 2*y);
    assert(array_equal(r.coef, [1, 2 ; 1, 2], eps), 'example A');
end

function test_distribute_over_eval()
    rand('seed', 42);
    p = MVPolyCube(rand(3, 3, 3));
    q = MVPolyCube(rand(4, 4, 4));
    pq = p*q;
    x0 = rand(3, 1);
    err = abs(1 - (p.eval(x0) * q.eval(x0)) / pq.eval(x0));
    assert(err < 2*eps, 'distribute over evaluation');
end
