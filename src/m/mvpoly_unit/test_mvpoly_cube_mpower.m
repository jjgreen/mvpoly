function test_mvpoly_cube_mpower()
% test suite for @MVPolyCube/mpower

    test_mpower_zero;
    test_mpower_one;
    test_mpower_two;
    test_composite;

end

function test_mpower_zero()
    p = MVPolyCube(rand(5));
    q = p^0;
    assert(array_equal(q.coef, [1], eps), 'mpower zero');
end

function test_mpower_one()
    p = MVPolyCube(rand(5));
    q = p^1;
    assert(array_equal(q.coef, p.coef, eps), 'mpower one');
end

function test_mpower_two()
    p = MVPolyCube(rand(5));
    q = p^2;
    r = p*p;
    assert(array_equal(q.coef, r.coef, eps), 'mpower two');
end

function test_composite()
    [x, y] = MVPolyCube.variables;
    p = (x + y^2)^2;
    c = [0 0 0 0 1
         0 0 2 0 0
         1 0 0 0 0];
    assert(array_equal(p.coef, c, eps), 'composite');
end
