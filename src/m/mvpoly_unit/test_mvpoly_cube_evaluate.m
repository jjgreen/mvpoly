function test_mvpoly_cube_evaluate()
% test suite for @MVPolyCube/eval

    test_univariate;
    test_bivariate;
    test_trivariate;
    test_regression_A;
    test_regression_B;
    test_regression_C;

end

function test_univariate()
    test_univariate_point;
end

function test_univariate_point()
    x = MVPolyCube.variables;
    p = x^2 - 1;
    assert(p.eval(2) == 3, 'univariate');
end

function test_bivariate()
    [x, y] = MVPolyCube.variables;
    p = x^2 + 2*y^2 - 1;
    test_bivariate_point(p);
    test_bivariate_block(p);
end

function test_bivariate_point(p)
    assert(p.eval(1, 2) == 8, 'bivariate point evaluation I');
    assert(p.eval([1, 2]') == 8, 'bivariate point evaluation II');
end

function test_bivariate_block(p)
    [x, y] = meshgrid(1:2);
    xy(1, :, :) = x;
    xy(2, :, :) = y;
    assert(array_equal(p.eval(x, y), [2, 5 ; 8, 11]), ...
           'bivariate grid evaluation I');
    assert(array_equal(p.eval(xy), [2, 5 ; 8, 11]), ...
           'bivariate grid evaluation II');
end

function test_trivariate()
    [x, y, z] = MVPolyCube.variables;
    p = x^2 + 2*y^2 + 3*z^2 - 1;
    assert(p.eval(1, 2, 3) == 35, ...
           'trivariate point evaluation I');
    assert(p.eval([1, 2, 3]') == 35, ...
           'trivariate point evaluation II');
end

function test_regression_A()
    [x, y, z] = MVPolyCube.variables;
    p = 2*x^2 + 3*y;
    assert(p.eval(1, 1) == 5, 'regression A');
end

function test_regression_B()
    [x, y, z] = MVPolyCube.variables;
    p = 2*x + 3*y^2;
    assert(p.eval(1, 1) == 5, 'regression B');
end

function test_regression_C()
    [x, y, z] = MVPolyCube.variables;
    p = 3*x^2 + y + z;
    assert(p.eval(1, 1, 1) == 5, 'regression C');
end