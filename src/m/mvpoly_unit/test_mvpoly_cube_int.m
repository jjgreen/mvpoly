function test_mvpoly_cube_int()
% test suite for @MVPolyCube/int

    test_monomial_x;
    test_monomial_y;
    test_derivative_inverse;
    test_vector_argument;

end

function test_monomial_x()
    x = MVPolyCube.variables;
    p = x^2;
    q1 = p.int(1);
    q2 = (1/3)*x^3;
    assert(array_equal(q1.coef, q2.coef, eps), 'monomial x^2');
end

function test_monomial_y()
    [x, y] = MVPolyCube.variables;
    p = y^2;
    q1 = p.int(0, 1);
    q2 = (1/3)*y^3;
    assert(array_equal(q1.coef, q2.coef, eps), 'monomial y^2');
end

function test_derivative_inverse()
    p = MVPolyCube(rand(5, 5, 5));
    for n = 1:3
        v = zeros(1, 3);
        v(n) = 1;
        q = p.int(v).diff(v);
        assert(array_equal(p.coef, q.coef, eps), 'derivative inverse');
    end
end

function test_vector_argument()
    p = MVPolyCube(rand(5, 5, 5));
    q1 = int(p, 1, 1, 2);
    q2 = int(p, [1, 1, 2]);
    assert(array_equal(q1.coef, q2.coef, eps), 'vector argument');
end