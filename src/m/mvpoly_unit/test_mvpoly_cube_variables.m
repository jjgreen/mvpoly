function test_mvpoly_cube_variables()
% test suite for @MVPolyCube/variables

    test_coefficients;
    test_composite;

end

function test_coefficients()
    [x, y] = MVPolyCube.variables;
    assert(x(1,0) == 1, 'variable x');
    assert(y(0,1) == 1, 'variable y');
end

function test_composite()
    [x, y] = MVPolyCube.variables;
    p = 2*x^2 + x*y + 1;
    m = [1 0 ; 0 1 ; 2 0];
    assert(array_equal(p.coef, m, eps), 'composite');
end