function x = norm(p, mu)
% NORM - the norm of the polynomial coefficients
%
% The norm of the coefficients of the polynomial, the value mu
% can be any value accepted by the built-in norm function.
%
% See also norm

    c = p.coef(:);
    x = builtin('norm', c, mu);

end
