function q = diff(p, varargin)
% DIFF - derivative of multivariate polynomial
%
% A call to
%
%    p.diff(n1, n2, ... nk)
%
% returns an MVPolyCube object which represents the
% n1-th derivative of p with respect to the first variable
% variable, the n2-th derivative with respect to the
% second variable and so on.  Thus diff() should have
% k arguments if p has k variables.
%
% Alternatively, one can call
%
%   p.diff([n1, ..., nk])
%
% for the same result.
%
% See also: @MVPolyCube/int

    pv = degrees(p);
    pn = numel(pv);
    k = nargin - 1;

    switch k
      case 1
        d = varargin{1};
        if numel(d) == pn
            q = diff1(p, d);
        else
            error('diff: expected 2nd arg to be %i-vector', pn);
        end
      case pn
        d = cell2mat(varargin);
        q = diff1(p, d);
      otherwise
        error('diff: expected %i arguments', pn+1);
    end
end

function q = diff1(p, d)

    pv = degrees(p);
    pn = numel(pv);
    q  = p;

    for k = 1:pn
        if d(k) > pv(k)
            q = MVPolyCube([0]);
            return
        end
        for n = 1:d(k)
            q = diff2(q, k);
        end
    end

end

function q = diff2(p, k)

    pv = degrees(p);
    pn = numel(pv);

    idx = colons(pn);
    idx{k} = 2:(pv(k) + 1);

    pc = p.coef;
    qc = pc(idx{:});
    qv = size(qc);
    rc = ndgridn(qv, k, 1:qv(k));
    q  = MVPolyCube(qc .* rc);

end
