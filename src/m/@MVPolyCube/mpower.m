function q = mpower(p, n)
% MPOWER - raise a polynomial to an integer power
%
% A call to mpower(p, n), or equivalently to p^n,
% returns the mutivariate polynomial p*p...*p where
% the usual mutiplication of polynomials is applied.

  if nargin ~= 2
    error('mpower requires exactly 2 arguments');
  end

  if n < 0
    error('mpower needs a non-negative power');
  end

  n = uint32(n);

  if n == 0
    q = MVPolyCube([1]);
    return;
  end

  q = binary_mpower(p, n);

end

function q = binary_mpower(p, n)
  if n == 1
    q = p;
  elseif mod(n, 2) == 0
    p = binary_mpower(p, idivide(n, 2));
    q = p * p;
  else
    q = p * binary_mpower(p, n - 1);
  end
end
