function a = ndgridn(sz, n, v)
% NDGRIDN - generate the n-th return value of NDGRID
%
% Given a vector of m sizes sz = [s1 s2 ... sm], a
% specified dimension n and a vector v with sn elements
% then
%
%    a = ndgridn(sz, n, v);
%
% generate the ND-array a which has v replicated in the
% n-th dimension.  Thus it it rather like the n-th return
% value to ndgrid, but without constructing its the other
% return values.
%
% For example, ndgridn([3 3], 1, 1:3) produces
%
%   1   1   1
%   2   2   2
%   3   3   3
%
% while ndgridn([3 3], 2, 1:3) gives
%
%   1   2   3
%   1   2   3
%   1   2   3
%
% See also ndgrid

    if nargin ~= 3
        error('ndgridn: 3 arguments required');
    end
    m = numel(sz);
    if n<1 || n>m
        error('ndgridn(sz, n, v) : n should be between 1 .. %i', m);
    end
    if sz(n) ~= numel(v)
        error('ndgridm(sz, n, v) : sz(n) should be size of v');
    end

    c = cell(m, 1);
    for i = 1:m
        c{m-i+1} = ones(sz(i), 1);
    end
    c{m-n+1} = v(:);

    a = reshape(MVPoly.kronn(c{:}), sz);

end
