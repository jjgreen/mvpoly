function q = ctranspose(p)
% CTRANSPOSE - multivariate polynomial with conjugate coefficients
%
% A call to
%
%   q = ctranspose(p)
%
% or equivalently q = p', returns the polynomial q whose
% coefficients are the complex conjugate of those of p.
% Note that the 'transpose' part of the name is deceptive,
% it is not relevant for a multivariate polynomial.

    c = p.coef;
    q = MVPolyCube(reshape(c(:)', size(c)));

end