function q = polyder(p, varargin)
% POLYDER - polynomial derivative
%
% alias for DIFF
    
    q = diff(p, varargin{:});
    
end