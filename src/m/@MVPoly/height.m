function x = height(p)
% HEIGHT - the largest absolute coefficient

    x = p.norm(Inf);

end
