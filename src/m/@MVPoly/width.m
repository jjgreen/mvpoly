function x = width(p)
% WIDTH - the sum of absolute values of polynomial coefficients

    x = p.norm(1);

end
