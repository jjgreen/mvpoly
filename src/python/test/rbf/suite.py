import pytest
import mvpoly.util.version as version
from mvpoly.cube import MVPolyCube
from mvpoly.dict import MVPolyDict
from mvpoly.rbf import (
    RBFGaussian,
    RBFMultiQuadric,
    RBFInverseMultiQuadric,
    RBFThinPlateSpline,
    RBFWendland
)

poly_classes = [
    MVPolyCube,
    MVPolyDict
]

rbf_classes = [
    RBFGaussian,
    RBFMultiQuadric,
    RBFInverseMultiQuadric,
    RBFThinPlateSpline,
    RBFWendland
]

rbf_eps_classes = [
    RBFGaussian,
    RBFMultiQuadric,
    RBFInverseMultiQuadric,
]

rbf_radius_classes = [
    RBFWendland
]

# silently skip Wendland RBF tests for old scipy versions,
# dirty hack needed until travis gets a more recent scipy

if not version.at_least('scipy', '0.17.0'):
    rbf_classes.remove(RBFWendland)
    rbf_radius_classes.remove(RBFWendland)


@pytest.fixture(params=poly_classes)
def poly_class(request):
    return request.param

@pytest.fixture(params=rbf_classes)
def rbf_class(request):
    return request.param

@pytest.fixture(params=rbf_eps_classes)
def rbf_eps_class(request):
    return request.param

@pytest.fixture(params=rbf_radius_classes)
def rbf_radius_class(request):
    return request.param
