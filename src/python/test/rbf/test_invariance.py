from . suite import rbf_class, poly_class
import pytest
import numpy as np
from mvpoly.rbf import RBFWendland

@pytest.fixture(params=[0, 1e-3, 1e-1])
def smooth(request):
    return request.param


# Check that a vertical shift of interpolation points gives
# rise to the same shift of the interpolation (or of the
# approximant, when smooth != 0).


def test_shift_invariance(rbf_class, poly_class, smooth):
    eps = 1e-10
    n = 10
    x = np.linspace(0, n-1, n)
    f = np.random.randn(len(x))
    rbf0 = rbf_class(x, f, smooth=smooth, poly_class=poly_class)
    rbf1 = rbf_class(x, f + 1, smooth=smooth, poly_class=poly_class)
    x_fine = np.linspace(0, n-1, 5*n)
    f0 = rbf0(x_fine) + 1
    f1 = rbf1(x_fine)
    assert np.allclose(f0, f1, atol=eps)


# Check that an affine transform of interpolation points
# gives rise to the same for the interpolation (or of the
# approximant, when smooth != 0).


def test_affine_invariance(rbf_class, poly_class, smooth):
    eps = 1e-10
    n = 10
    x = np.linspace(0, n-1, n)
    f = np.random.randn(len(x))
    rbf0 = rbf_class(x, f, smooth=smooth, poly_class=poly_class)
    rbf1 = rbf_class(x, f + 3*x + 2, smooth=smooth, poly_class=poly_class)
    x_fine = np.linspace(0, n-1, 5*n)
    f0 = rbf0(x_fine) + 3*x_fine + 2
    f1 = rbf1(x_fine)
    assert np.allclose(f0, f1, atol=eps)
