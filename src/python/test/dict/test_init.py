from mvpoly.dict import MVPolyDict, MVPolyDictMonomial
import numpy as np
import pytest


def test_construct_from_empty():
    exp = {}
    obt = MVPolyDict().coef
    assert isinstance(obt, dict)
    assert exp == obt


def test_construct_from_dict():
    exp = {((2, 3),): 9,
           ((1, 2),): 8}
    obt = MVPolyDict(exp, dtype=int).coef
    assert exp == obt


def test_construct_from_variables():
        x, y = MVPolyDict.variables(2)
        p = x + 2*y + 3
        exp = {((0, 1),): 1,
               ((1, 1),): 2,
               (): 3}
        obt = p.coef
        assert exp == obt


def test_construct_from_variables_zero_coefs():
        x, y = MVPolyDict.variables(2)
        p = 0*x + 0*y + 0
        exp = {}
        obt = p.coef
        assert exp == obt
