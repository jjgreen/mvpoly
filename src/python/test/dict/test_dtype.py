from mvpoly.dict import MVPolyDict
import numpy as np
import pytest


@pytest.fixture
def d():
    return {((2, 3),): 9, ((1, 2),): 8}


@pytest.fixture
def pi(d):
    return MVPolyDict(d.copy(), dtype=np.int64)


@pytest.fixture
def pf(d):
    return MVPolyDict(d, dtype=np.float32)


def test_coef_explict(pi, pf):
    assert pf[0, 2].dtype == np.float32
    assert pi[0, 2].dtype == np.int64


def test_coef_implict(pi, pf):
    assert pf[0, 0].dtype == np.float32
    assert pi[0, 0].dtype == np.int64


def test_get_dtype(pi, pf):
    assert pf.dtype == np.float32
    assert pi.dtype == np.int64


def test_set_dtype(pi, pf):
    pf.dtype = bool
    assert pf.dtype == bool
    pi.dtype = bool
    assert pi.dtype == bool


def test_set_coef_dtype(pf):
    pf[0, 7] = 4
    assert pf[0, 7].dtype == np.float32


def test_persist(pi):
    for q in [
            pi + pi,
            pi + 1,
            1 + pi,
            pi * pi,
            2 * pi,
            pi * 2,
            pi**2
    ]:
        assert q.dtype == np.int64
