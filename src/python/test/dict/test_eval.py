from mvpoly.dict import MVPolyDict
import numpy as np
import pytest


def makep(x, y):
    return x**2 + 2*x*y + 3*y + 4*y**2 + 5


@pytest.fixture
def p():
    x, y = MVPolyDict.variables(2, dtype=int)
    return makep(x, y)


@pytest.fixture
def x():
    return [1, 1, -1, 0,  7, 3,  -3, 1]


@pytest.fixture
def y():
    return [1, 0,  0, 3, -1, 2, -10, 2]


def test_eval_point(p, x, y):
    for xi, yi in zip(x, y):
        assert p.eval(xi, yi) == makep(xi, yi)


def test_eval_array_1d(p, x, y):
    obt = p.eval(x, y)
    exp = [makep(x[i], y[i]) for i in range(len(x))]
    assert (exp == obt).all()


def test_eval_array_2d(p, x, y):
    n = len(x)
    m = n // 2
    X = np.reshape(x, (2, m))
    Y = np.reshape(y, (2, m))
    obt = p.eval(X, Y)
    assert obt.shape == (2, m)
    obt.shape = (n,)
    exp = [makep(x[i], y[i]) for i in range(n)]
    assert (exp == obt).all()


def test_eval_badargs(p, x, y):
    with pytest.raises(ValueError):
        p.eval(x[1:], y)
