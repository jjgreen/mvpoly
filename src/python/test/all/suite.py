import pytest
from mvpoly.cube import MVPolyCube
from mvpoly.dict import MVPolyDict


TEST_CLASSES = [
    MVPolyCube,
    MVPolyDict
]


@pytest.fixture(params=TEST_CLASSES)
def cls(request):
    return request.param
