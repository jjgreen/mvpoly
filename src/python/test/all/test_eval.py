from . suite import cls
import pytest
import numpy as np


@pytest.fixture(params=[int, float])
def dtype(request):
    return request.param


def test_eval_zero_scalar(cls):
    p = cls.zero()
    assert p(2) == 0


def test_eval_zero_scalars(cls):
    p = cls.zero()
    assert p(2, 4) == 0


def test_eval_zero_array(cls):
    a = np.array([[4, 5]])
    b = np.array([[0, 0]])
    p = cls.zero()
    assert np.array_equal(p(a), b)


def test_eval_one_scalar(cls):
    p = 3 * cls.one()
    assert p(2) == 3


def test_eval_one_scalars(cls):
    p = 3 * cls.one()
    assert p(2, 4, 7) == 3


def test_eval_one_array(cls):
    a = np.array([[4, 5]])
    b = np.array([[3, 3]])
    p = 3 * cls.one()
    assert np.array_equal(p(a), b)


def test_eval_dtype(cls, dtype):
    x, y = cls.variables(2, dtype=dtype)
    p = x + y + 1
    assert p.dtype == dtype
    xg = np.array([1, 2], dtype=dtype)
    yg = np.array([-2, 0], dtype=dtype)
    assert p(xg, yg).dtype == dtype


def test_eval_regression(cls, dtype):
    x, y = cls.variables(2, dtype=dtype)
    p = 2*x**2 + 3*y
    assert p(1, 1) == 5
    p = 2*x + 3*y**2
    assert p(1, 1) == 5
    x, y, z = cls.variables(3, dtype=dtype)
    p = 3*x**2 + y + z
    assert p(1, 1, 1) == 5


def test_eval_univar(cls):
    x, = cls.variables(1)
    p = x**2 + 1
    assert p(2) == 5


def test_eval_1d_in_2d(cls):
    x, _ = cls.variables(2, dtype=int)
    p = x**2 + 1
    assert p.eval(2, 1) == 5


def test_eval_return_type(cls):
    x, y = cls.variables(2, dtype=int)
    p = x**2 + y
    assert isinstance(p.eval(2, 1), np.int64)


def test_eval_complex(cls):
    x, y = cls.variables(2, dtype=complex)
    p = x**2 - y
    assert p.eval(1.0j, 1.0) == -2
