from . suite import cls, TEST_CLASSES
import itertools
import numpy as np


def test_roundtrip():
    perms = itertools.permutations(TEST_CLASSES, 2)
    for perm in perms:
        for dtype in [int, float, np.float64]:
            x, y = perm[0].variables(2, dtype=dtype)
            p = (x + y + 1)**3
            q = p.asclass(perm[1])
            r = q.asclass(perm[0])
            assert p == r
            assert p.dtype == r.dtype
