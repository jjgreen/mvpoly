from . suite import cls
import pytest
import warnings
import numpy as np


@pytest.fixture(params=[np.float32, np.float64, complex])
def dtype(request):
    return request.param


def test_diff_0d(cls):
    x, = cls.variables(1, dtype=np.double)
    p = 3 * cls.one(dtype=np.double)
    assert p.int(1) == 3*x


def test_int_1d(cls):
    x, = cls.variables(1, dtype=np.double)
    p = 5*x**4 + 4*x
    assert p.int(1) == x**5 + 2*x**2


def test_int2_1d(cls):
    x, = cls.variables(1, dtype=np.double)
    p = 6*x + 2
    assert p.int(2) == (x + 1) * x**2


def test_int_2d_x(cls):
    x, y = cls.variables(2, dtype=np.double)
    p = 3*x**2 + 4*x*y + 3*y**2
    assert p.int(1, 0) == x**3 + 2*y*(x**2) + 3*x*y**2


def test_int_2d_y(cls):
    x, y = cls.variables(2, dtype=np.double)
    p = 3*x**2 + 4*x*y + 3*y**2
    assert p.int(0, 1) == 3*y*x**2 + 2*x*y**2 + y**3


def test_int_invariant(cls):
    x, = cls.variables(1, dtype=np.double)
    p0 = 2*x
    p1 = 2*x
    p0.int(1)
    assert p0 == p1


def test_int_dtype(cls, dtype):
    x, y = cls.variables(2, dtype=int)
    p = (x+y)**2
    q = p.int(1, 1, dtype=dtype)
    assert q.dtype == dtype


def test_int_integer_warning(cls):
    x, y = cls.variables(2, dtype=int)
    p = 3*x + 4*y
    with warnings.catch_warnings(record=True) as w:
        warnings.simplefilter('always')
        p.int(1, 1)
        assert len(w)
        assert issubclass(w[0].category, RuntimeWarning)
