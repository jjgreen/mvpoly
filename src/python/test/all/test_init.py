from . suite import cls, TEST_CLASSES
import itertools
import numpy as np


def test_sameclass(cls):
    x, y = cls.variables(2)
    p = (x + 2*y - 4)**3
    q = cls(p)
    assert p == q


def test_roundtrip():
    perms = itertools.permutations(TEST_CLASSES, 2)
    for perm in perms:
        for dtype in [int, float, np.float64]:
            x, y = perm[0].variables(2, dtype=dtype)
            p = (x + y + 1)**3
            q = perm[1](p)
            r = perm[0](q)
            assert p == r
            assert p.dtype == r.dtype


def test_default_dtype(cls):
    default = np.double
    x, y = cls.variables(2)
    assert x.dtype == default
    assert y.dtype == default
    assert cls.zero().dtype == default
    assert cls.one().dtype == default
