from . suite import cls
import pytest
import numpy as np


def test_div_scalar(cls):
    x, y = cls.variables(2, dtype=int)
    p1 = (2*x + 6*y)/2
    p2 = (3*x + 7*y)/2
    q = x + 3*y
    assert p1 == q
    assert p2 == q


def test_div_poly_raises_typerror(cls):
    # Division of polynomials is not supported in this
    # library, since in the multivariate case it involves
    # Gröbner basis calculation (which is unstable for
    # floating-point coefficients). There is research on
    # stabilised (regularised) Gröbner bases but adding
    # that work to this library would be a major project.
    x, y = cls.variables(2, dtype=int)
    with pytest.raises(TypeError):
        x / y
    with pytest.raises(TypeError):
        x / x
