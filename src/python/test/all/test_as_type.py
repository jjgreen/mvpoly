from . suite import cls


def test_conversion_to_float(cls):
    x, y = cls.variables(2, dtype=int)
    p = (x + 2*y - 4)**3
    assert p[3, 0] == 1
    q = p.astype(float)
    assert q[3, 0] == 1.0


def test_conversion_to_int(cls):
    x, y = cls.variables(2, dtype=float)
    p = (x + 2*y - 4)**3
    assert p[3, 0] == 1.0
    q = p.astype(float)
    assert q[3, 0] == 1


def test_roundtrip(cls):
    x, y = cls.variables(2, dtype=int)
    p = (x + 2*y - 4)**3
    q = p.astype(float)
    r = q.astype(int)
    assert p == r


def test_dtype_property(cls):
    x, y = cls.variables(2, dtype=int)
    p = (x + 2*y - 4)**3
    q = p.astype(float)
    assert q.dtype == float
