# Property-tests courtesy of D. R. MacIver's Hypothesis
# http://hypothesis.works/

from mvpoly.cube import MVPolyCube
from mvpoly.dict import MVPolyDict

import numpy as np
import pytest
import warnings
import os

warnings.simplefilter(action='ignore', category=FutureWarning)

from hypothesis import given, settings, strategies as st
from hypothesis.extra.numpy import arrays


settings.register_profile(
    'ci',
    settings(
        deadline=None,
        max_examples=100
    )
)

settings.register_profile(
    'development',
    settings(
        deadline=None,
        max_examples=10
    )
)

settings.load_profile(os.getenv('MVPOLY_ENV', 'ci'))

# Test classes specialised by underlying data structure and
# data type.  We want to use as little as possible of the
# internal machinery of the object, instead get to the
# underlying data representation and then make assertions on
# that.  The split on data-type is since in the integer case
# we obtain (and so can assert) exact results.

eps = 1e-16

class PropertyTest(object):

    def assert_poly_eq(self, p1, p2, **kwarg):
        raise NotImplementedError()

    def assert_poly_zero(self, p, **kwargs):
        raise NotImplementedError()


class PropertyTestIntCube(PropertyTest):

    def assert_poly_eq(self, p1, p2, **kwargs):
        self.assert_poly_zero(p1 - p2)

    def assert_poly_zero(self, p, **kwargs):
        self._assert_array_zero(p.coef)

    @staticmethod
    def _assert_array_zero(a):
        assert np.all(a == 0)


class PropertyTestFloatCube(PropertyTest):

    def assert_poly_eq(self, p1, p2, eps=eps):
        dp = p1 - p2
        shp = dp.coef.shape
        z = MVPolyCube(np.zeros(shp, dtype=dp.dtype), dtype=dp.dtype)
        a1 = (p1 + z).coef
        a2 = (p2 + z).coef
        assert np.all(np.isclose(a1, a2, rtol=eps))

    def assert_poly_zero(self, p, eps=eps):
        self.assert_poly_eq(p, MVPolyCube.zero(dtype=p.dtype), eps=eps)


class PropertyTestIntDict(PropertyTest):

    def assert_poly_eq(self, p1, p2, **kwargs):
        assert p1.coef == p2.coef

    def assert_poly_zero(self, p, eps=eps):
        self.assert_poly_eq(p, MVPolyDict.zero(dtype=p.dtype))


class PropertyTestFloatDict(PropertyTest):

    def assert_poly_eq(self, p1, p2, eps=eps):
        for idx, _ in (p1 - p2).nonzero:
            p1i = p1[idx]
            p2i = p2[idx]
            pmax = max(abs(p1i), abs(p2i))
            assert abs(p1i - p2i) <= pmax * eps

    def assert_poly_zero(self, p, eps=eps):
        self.assert_poly_eq(p, MVPolyDict.zero(dtype=p.dtype), eps=eps)


# the parameterised tests

def suite(poly, mvpoly_class, test_class):

    class RingAxioms(test_class):

        @given(poly, poly, poly)
        def test_addition_associative(self, p, q, r):
            self.assert_poly_eq((p + q) + r, p + (q + r), eps=1e-15)

        @given(poly)
        def test_additive_identity(self, p):
            z = mvpoly_class.zero(dtype=p.dtype)
            self.assert_poly_eq(z + p, p)
            self.assert_poly_eq(p + z, p)

        @given(poly)
        def test_additive_inverse(self, p):
            self.assert_poly_zero(p - p)

        @given(poly, poly)
        def test_addition_commutative(self, p, q):
            self.assert_poly_eq(p + q, q + p)

        @pytest.mark.skip(reason="counterexamples for eps 1e-3")
        @given(poly, poly, poly)
        def test_multiplication_associative(self, p, q, r):
            self.assert_poly_eq((p * q) * r, p * (q * r), eps=1e-2)

        @given(poly)
        def test_mutiplicative_identity(self, p):
            one = mvpoly_class.one(dtype=p.dtype)
            self.assert_poly_eq(one * p, p)
            self.assert_poly_eq(p * one, p)

        @given(poly, poly)
        def test_multiplication_commutative(self, p, q):
            self.assert_poly_eq(p * q, q * p,  eps=1e-13)

        @pytest.mark.skip(reason="counterexamples founs")
        @given(poly, poly, poly)
        def test_distributive(self, p, q, r):
            self.assert_poly_eq(p * (q + r), p * q + p * r, eps=1e-10)
            self.assert_poly_eq((p + q) * r, p * r + q * r, eps=1e-10)

    return RingAxioms


# cube test-suites

def poly_strategy_cube(dtype, element_strategy):
    return st.builds(
        MVPolyCube,
        arrays(
            dtype,
            (2, 2),
            elements = element_strategy
        ),
        dtype = st.just(dtype)
    )

def suite_cube(dtype, element_strategy, property_test):
    poly = poly_strategy_cube(dtype, element_strategy)
    return suite(poly, MVPolyCube, property_test)

def suite_int_cube(dtype, element_strategy):
    return suite_cube(dtype, element_strategy, PropertyTestIntCube)

def suite_float_cube(dtype, element_strategy):
    return suite_cube(dtype, element_strategy, PropertyTestFloatCube)


class TestRingAxiomsIntCube(
        suite_int_cube(
            int,
            st.integers(-1000, 1000)
        )
):
    pass

class TestRingAxiomsFloatCube(
        suite_float_cube(
            float,
            st.floats(-1000, 1000)
        )
):
    pass

class TestRingAxiomsComplexCube(
        suite_float_cube(
            complex,
            st.complex_numbers(min_magnitude=1e-3, max_magnitude=1e3)
        )
):
    pass


# dict test-suites

def poly_strategy_dict(dtype, element_strategy):
    return st.builds(
        MVPolyDict.init_from_nonzero_tuples,
        st.lists(
            max_size = 10,
            elements = st.tuples(
                st.tuples(
                    st.integers(0, 20),
                    st.integers(0, 20),
                    st.integers(0, 20)
                ),
                element_strategy
            )
        ),
        dtype = st.just(dtype)
    )

def suite_dict(dtype, element_strategy, property_test):
    poly = poly_strategy_dict(dtype, element_strategy)
    return suite(poly, MVPolyDict, property_test)

def suite_int_dict(dtype, element_strategy):
    return suite_dict(dtype, element_strategy, PropertyTestIntDict)

def suite_float_dict(dtype, element_strategy):
    return suite_dict(dtype, element_strategy, PropertyTestFloatDict)


class TestRingAxiomsIntDict(
        suite_int_dict(
            int,
            st.integers(-1000, 1000)
        )
):
    pass

class TestRingAxiomsFloatDict(
        suite_float_dict(
            float,
            st.floats(-1000, 1000)
        )
):
    pass

class TestRingAxiomsComplexDict(
        suite_float_dict(
            complex,
            st.complex_numbers(min_magnitude=1e-3, max_magnitude=1e3)
        )
):
    pass
