from mvpoly.cube import MVPolyCube
import pytest


@pytest.fixture
def A():
    return MVPolyCube([1, 1], dtype=int)


@pytest.fixture
def B():
    return MVPolyCube([[1, 1], [1, 1]], dtype=int)


def test_scalar(A):
    obtained = (2 * A).coef
    expected = [2, 2]
    assert (expected == obtained).all()


def test_univariate(A):
    obtained = (A * A).coef
    expected = [1, 2, 1]
    assert (expected == obtained).all()


def test_dtype(A):
    C = A * A
    assert C.dtype == int


def test_bivariate(A, B):
    expected = [[1, 1],
                [2, 2],
                [1, 1]]
    assert ((A * B).coef == expected).all()
    assert ((B * A).coef == expected).all()


def test_arithmetic():
    x, y = MVPolyCube.variables(2, dtype=int)
    p1 = (x + y)*(2*x - y)
    p2 = 2*x**2 + x*y - y**2
    assert (p1.coef == p2.coef).all()


def test_complex():
    x, y = MVPolyCube.variables(2, dtype=complex)
    p1 = (x + y)*(x + 1j*y)
    p2 = x**2 + (1 + 1j)*x*y + 1j*y**2
    assert (p1.coef == p2.coef).all()
