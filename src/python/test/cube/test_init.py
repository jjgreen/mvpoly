from mvpoly.cube import MVPolyCube
import pytest


def test_from_empty():
    assert (MVPolyCube().coef == []).all()
