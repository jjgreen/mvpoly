from mvpoly.cube import MVPolyCube
import numpy as np
import pytest


dtypes = [
    float,
    int,
    complex,
    np.int32,
    np.int64,
    np.float32,
    np.float64,
    np.complex64,
    np.complex128,
]


@pytest.fixture(params=dtypes)
def dtype_poly(request):
    return (request.param, MVPolyCube([1, 2], dtype=request.param))


def test_get_dtype(dtype_poly):
    dtype, poly = dtype_poly
    assert poly.dtype == dtype


def test_set_dtype(dtype_poly):
    _, poly = dtype_poly
    assert poly.dtype != bool
    poly.dtype = bool
    assert poly.dtype == bool


p = MVPolyCube([1, 2], dtype=int)


@pytest.mark.parametrize(
    'q',
    [
        p + p,
        p - p,
        p + 1,
        1 + p,
        p * p,
        2 * p,
        p * 2,
        p**3
    ]
)
def test_persist(q):
    assert q.dtype == int
