from mvpoly.cube import MVPolyCube
import pytest


def makep(x, y):
    return (1 - x**2) * (1 + y) - 8


@pytest.fixture
def p():
    x, y = MVPolyCube.variables(2, dtype=int)
    return makep(x, y)


@pytest.mark.parametrize(
    'x,y',
    [
        (0, 0),
        (1, 0),
        (-1, 0),
        (0, 3),
        (7, -1),
        (3, 3),
        (-3, -10),
        (1, 2)
    ]
)
def test_point(p, x, y):
    assert p(x, y) == makep(x, y)


def test_array_1d(p):
    x = [1, 2]
    y = [3, 4]
    obtained = p(x, y)
    expected = [makep(x[i], y[i]) for i in range(len(x))]
    assert (expected == obtained).all()


def test_array_2d(p):
    x = [[1, 2],
         [3, 4]]
    y = [[5, 6],
         [7, 8]]
    obtained = p(x, y)
    expected = [
        [makep(x[i][j], y[i][j]) for j in range(2)]
        for i in range(2)
    ]
    assert (expected == obtained).all()
