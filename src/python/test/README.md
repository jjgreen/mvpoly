Tests
=====

This suite switched from the venerable _unittest_ at a time when it
was deprecated (this no longer seems to be the case, some kind soul
seems to have stepped in to maintain it), it now uses  _pytest_ (I
think that's a good move in any case).  Partly as as a result of this
we has some peculiar dependencies:

- We need _hypothesis_ at least 3.54, for magnitude
  constrained complex numbers

- To run on Ubuntu Xenial, we need _pytest-runner_ prior
  to 4.2, this because that version requires sutuptools
  later than 20.7.0 distributed with the OS.  For some
  reason, this does not appear to be an issue on travis.

These should be relaxed as things stabilise and get tweaked.
