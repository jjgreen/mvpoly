import numpy as np

import pytest
from fixtures import cases

from mvpoly.util.dtype import dtype_mul


def test_dtype_add(cases):
    for t1, t2, expected in cases:
        assert dtype_mul(t1, t2) == expected
