import scipy.special as sps
from mvpoly.util.common import binom
import pytest


def test_scipy_special():
    for n, i in [(3, 2), (5, 0), (7, 6)]:
        a = binom(n, i)
        b = int(round(sps.binom(n, i)))
        assert a == b
