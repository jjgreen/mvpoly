import numpy as np
import pytest
from mvpoly.util.cube import padded_sum


def test_simple():
    A = np.array(
        [1, 2, 3],
        dtype=int
    )
    B = np.array(
        [[1],
         [1]],
        dtype=int
    )
    Co = padded_sum(A, B, dtype=int)
    Cx = np.array(
        [[2, 2, 3],
         [1, 0, 0]],
        dtype=int
    )
    assert (Co == Cx).all()
    assert Co.dtype == Cx.dtype

def test_mixed_dtypes():
    A = np.array(
        [1, 2, 3],
        dtype=int
    )
    B = np.array(
        [[1],
         [1]],
        dtype=float
    )
    Co = padded_sum(A, B, dtype=float)
    Cx = np.array(
        [[2, 2, 3],
         [1, 0, 0]],
        dtype=float
    )
    assert (Co == Cx).all()
    assert Co.dtype == Cx.dtype

def test_missing_dtype():
    A = np.array([1], dtype=int)
    with pytest.raises(TypeError):
        padded_sum(A, A)

def test_unknown_keyword():
    A = np.array([1], dtype=int)
    with pytest.raises(TypeError):
        padded_sum(A, A, dtype=int, foo='bar')
