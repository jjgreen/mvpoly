import pytest
from mvpoly.util.cached_property import cached_property


class Example(object):

    def __init__(self):
        self._count = 0

    @property
    def count(self):
        self._count += 1
        return self._count

    @cached_property
    def cached_count(self):
        return self.count


@pytest.fixture
def example():
    return Example()


def test_counter(example):
    assert example.count == 1
    assert example.count == 2
    assert example.count == 3


def test_caches(example):
    assert example.cached_count == 1
    assert example.cached_count == 1


def test_assign_raises(example):
    assert example.cached_count == 1
    with pytest.raises(AttributeError):
        example.cached_count = 7


def test_cached_delete_racalculates(example):
    assert example.cached_count == 1
    del example.cached_count
    assert example.cached_count == 2
    assert example.cached_count == 2
