# some tools for versions of libraries

import scipy.version
import numpy.version
import re


# The next few functions are extracted from pkg_resources.py
# in the standard library but now deprecated in favour of a
# third-party library.  We just want the version-comparison
# facility, so find it easier to extract the old code rather
# than introduce a new dependency ... these at licenced under
# the PSL licence https://docs.python.org/3/license.html which
# is compatible with the LGPL licence of this package.

component_re = re.compile(r'(\d+ | [a-z]+ | \.| -)', re.VERBOSE)
replace = {
    'pre':'c',
    'preview':'c',
    '-':'final-',
    'rc':'c',
    'dev':'@'
}.get

def _parse_version_parts(s):
    for part in component_re.split(s):
        part = replace(part, part)
        if not part or part=='.':
            continue
        if part[:1] in '0123456789':
            yield part.zfill(8)
        else:
            yield '*' + part
    yield '*final'

def _parse_version(s):
    parts = []
    for part in _parse_version_parts(s.lower()):
        if part.startswith('*'):
            if part < '*final':
                while parts and parts[-1] == '*final-':
                    parts.pop()
            while parts and parts[-1] == '00000000':
                parts.pop()
        parts.append(part)
    return tuple(parts)

def at_least(libstr, req):
    if libstr == 'scipy':
        ver = scipy.version.version
    elif libstr == 'numpy':
        ver = numpy.version.version
    else:
        raise ValueError("library {0!s} not known".format((libstr)))
    return _parse_version(req) <= _parse_version(ver)
