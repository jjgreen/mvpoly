# mvpoly

A library for the numeric treatment of multivariate polynomials.


## Objective

To provide a number of basic operations (addition, multiplication,
evaluation) on dense and sparse multivariate polynomials in a
uniform and intuitive manner in a number of high-level numerical
programming environments (Python, Octave/Matlab).


### Example

In the following Octave session we create the polynomial p(x, y) = 1 + x + 2y

``` matlab
p = MVPolyCube;
p(0, 0) = p(1, 0) = 1;
p(0, 1) = 2;
```

alternatively one can assign the coefficients directly

``` matlab
p = MVPolyCube([1, 2; 1, 0]);
```

or generate the polynomial from variables, in an "almost symbolic" manner

``` matlab
[x, y] = MVPolyCube.variables;
p = 2 * y + x + 1;
```

Evaluate it at x = y = 1:

``` matlab
p.eval(1, 1)
```

Create data for plotting the same polynomial on the square

``` matlab
L = linspace(-1, 1, 20);
[X, Y] = meshgrid(L, L);
z = p.eval(X, Y);
```


## Status

Stable, maintained and [tested][1] against Octaves 6–9 and Pythons
3.8–3.13.


## More information

* Package [homepage][2]
* The [Python documentation][3]
* The pip package at [GitLab][4]


## Author

[J.J. Green][5]

[1]: https://gitlab.com/jjg/mvpoly/pipelines
[2]: https://jjg.gitlab.io/en/code/mvpoly/
[3]: https://mvpoly.readthedocs.io/en/latest/
[4]: https://gitlab.com/jjg/mvpoly/-/packages
[5]: https://jjg.gitlab.io/
